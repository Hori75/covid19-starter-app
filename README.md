# COVID-19 Starter App

Aplikasi web ini dibuat untuk membantu pelajar mendapatkan lokasi belajar yang efektif
jika mereka tidak bisa belajar di rumah. Aplikasi ini juga menyediakan informasi tentang 
info COVID-19, berita penting, dan daftar rumah sakit rujukan.

## Fitur yang akan diimplementasikan

 - Home (main page)
 - Daftar Rumah Sakit Rujukan
 - Tempat Belajar yang aman selama Pandemi
 - Forum

## Daftar anggota kelompok

 - (1906307132) Muchlisah Lusiambali 
 - (1906350521) William
 - (1906350976) Mochammed Daffa El Ghifari
 - (1906305455) Alfina Megasiwi

